package com.daugherty;

public interface IRecommendationRule {
	public boolean isInvestmentRecommended(int riskLevel, double returnRatio);
}
