package com.daugherty;

public class LowRiskDecentProfit implements IRecommendationRule {

    @Override
    public boolean isInvestmentRecommended(int riskLevel, double returnRatio) {
        return (riskLevel < 10 && returnRatio > 6);
    }
}
