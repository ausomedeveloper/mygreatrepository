package com.daugherty;

import java.util.ArrayList;

public class RecommendedInvestmentDisplayGUI {

	private RecommendationProcessor recommendationProcessor = new RecommendationProcessor();

    public void displayRecommendedInvestments() {
        ArrayList<Investment> recommendedInvestments = recommendationProcessor.findAllRecommendedInvestments();

        System.out.println("Recommended investments");
        System.out.println("-----------------------");
        for(int i = 0; i < recommendedInvestments.size(); i++) {
            System.out.println(recommendedInvestments.get(i).getDescription());
        }
    }
}
