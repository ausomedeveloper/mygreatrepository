package com.daugherty;

import java.util.ArrayList;

public class RecommendationProcessor {

	private InvestmentLoader investmentLoader = new InvestmentLoader();

    public ArrayList<Investment> findAllRecommendedInvestments() {

        Investment[] allInvestments = investmentLoader.getAllInvestments();
        ArrayList<Investment> recommendedInvestments = new ArrayList<Investment>();
        
        for(int i = 0; i < allInvestments.length; i++) {
            Investment currentInvestment = allInvestments[i];
            if(this.isInvestmentRecommended(currentInvestment)) {
                recommendedInvestments.add(currentInvestment);
            }
        }
        return recommendedInvestments;
    }

    public boolean isInvestmentRecommended(Investment investment) {
        int riskLevel = investment.getRiskLevel();
        double returnRatio = investment.getReturnRatio();
        
        IRecommendationRule high = new HighRiskHighProfit();
        if (high.isInvestmentRecommended(riskLevel, returnRatio)) {
        	return true;
        }
        IRecommendationRule low = new LowRiskDecentProfit();
        if(low.isInvestmentRecommended(riskLevel, returnRatio)) {
        	return true;
        }

        // Anything else isn't recommended with the current rules
        return false;
    }
}
