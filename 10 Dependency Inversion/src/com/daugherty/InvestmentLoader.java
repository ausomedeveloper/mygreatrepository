package com.daugherty;

public class InvestmentLoader {

    public Investment[] getAllInvestments() {
        // Would connect to a database and get a list of all possible investments
        return new Investment[] {
                new Investment("Risky business", 100, 200),
                new Investment("Startup", 50, 175),
                new Investment("Savings account", 1, 2),
                new Investment("Stocks account", 8, 7),
                new Investment("High-frequency trading", 40, 20)
        };
    }
}
