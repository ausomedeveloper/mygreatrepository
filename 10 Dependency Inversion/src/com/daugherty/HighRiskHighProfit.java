package com.daugherty;

public class HighRiskHighProfit implements IRecommendationRule {

	@Override
	public boolean isInvestmentRecommended(int riskLevel, double returnRatio) {
		return riskLevel < 75 && returnRatio > 150;
	}

}
